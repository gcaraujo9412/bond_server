from locust import HttpLocust, Locust, TaskSet, task, between
import json
import uuid
import random
import string
from random import randrange

class UserBehavior(TaskSet):



    def randomString(self,stringLength=10):
        """Generate a random string of fixed length """
        letters = string.ascii_lowercase
        return ''.join(random.choice(letters) for i in range(stringLength))

    @task(1)
    def create_post(self):

        names=["Goncalo","Araujo","Caldinhas","Claudio","Pires","Wendy","Tomates"]
        id=uuid.uuid1().time
        name=random.choice(names)
        val=self.randomString(10)
        add=name+val
        headers = {'content-type': 'application/json','Accept-Encoding':'gzip'}
        self.client.post("http://127.0.0.1:8080/alias",data= json.dumps({

            "newUserId": add,
            "originalUserId": name,
            "timestampUTC": id

    }),
    headers=headers,
    name = "Alias Post")

    @task(3)
    def create_post_2(self):

        names=["Goncalo","Araujo","Caldinhas","Claudio","Pires","Wendy","Tomates"]
        events=["clicked","opened","closed","stored"]
        id=uuid.uuid1().time
        name=random.choice(names)
        event=random.choice(events)
        val=self.randomString(6)
        url="http://www."+val+".com"
        headers = {'content-type': 'application/json','Accept-Encoding':'gzip'}
        self.client.post("http://127.0.0.1:8080/track",data= json.dumps({

            "userId": name,
            "events": [{
    			"eventName": event,
          		"metadata": {

    				"url":url,
    				"id":12312414


    			},
          		   "timestampUTC": id
                   }],




    		}),
    headers=headers,
    name = "Track Post")

    @task(4)
    def create_post_3(self):

        names=["Goncalo","Araujo","Caldinhas","Claudio","Pires","Wendy","Tomates"]
        lang=["PT","EN","ES","FR","GR","RUS"]
        id=uuid.uuid1().time
        name=random.choice(names)

        val1=randrange(100)

        val2=random.choice(lang)

        headers = {'content-type': 'application/json','Accept-Encoding':'gzip'}
        self.client.post("http://127.0.0.1:8080/profile",data= json.dumps({

            "userId": name,
            "attributes":{
    			"age":val1,
                "country":val2,
    			},
    		"timestampUTC": id

    	}),
    headers=headers,
    name = "Profile Post")


class WebsiteUser(HttpLocust):
    task_set = UserBehavior
    wait_time = between(1,3)
