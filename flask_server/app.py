from flask import Flask, render_template, request
import boto3
import json
import os
import uuid
import requests
from checkJson import jsonChecker as jCheck
from botocore.exceptions import ClientError
import logging

# Create app
app = Flask(__name__)

AWSAccessKeyId = ""
aall = ''
AWSSecretKey = ""
S3Client = ""
FirehoseClient = ""


# Home page
@app.route("/", methods=['GET'])
def home():

    return "Welcome to Bond_touch test server"


@app.route("/track", methods=['POST'])
def track():

    out = request.data
    value = out.decode("UTF-8")

    if jCheck.checkTrack(value):
        try:
            put = FirehoseClient.put_record(
                DeliveryStreamName='bond_stream',
                Record={
                    'Data': value
                }
            )
            print(put)
        except ClientError as e:
            logging.error(e)
            exit(1)
    else:
        return "Incorrect JSON format"

    return value


@app.route("/alias", methods=['POST'])
def alias():

    out = request.data
    value = out.decode("UTF-8")

    if jCheck.checkAlias(value):
        try:
            put = FirehoseClient.put_record(
                DeliveryStreamName='bond_stream',
                Record={
                    'Data': value
                }
            )
            print(put)
        except ClientError as e:
            logging.error(e)
            exit(1)
    else:
        return "Incorrect JSON format"

    return value


@app.route("/profile", methods=['POST'])
def profile():

    out = request.data
    value = out.decode("UTF-8")

    if jCheck.checkProfile(value):
        try:
            put = FirehoseClient.put_record(
                DeliveryStreamName='bond_stream',
                Record={
                    'Data': value
                }
            )
            print(put)
        except ClientError as e:
            logging.error(e)
            exit(1)

    else:
        return "Incorrect JSON format"

    return value


if __name__ == "__main__":

    with open(os.getenv("HOME") + "/aws_credentials/keys.txt") as r:

        for lines in r.readlines():
            if "AWSAccessKeyId" in lines:
                AWSAccessKeyId = lines.split("=")[1].replace('\n', "")
            if "AWSSecretKey" in lines:
                AWSSecretKey = lines.split("=")[1].replace('\n', "")

    FirehoseClient = boto3.client(
        'firehose',
        region_name='eu-west-2',
        aws_access_key_id=AWSAccessKeyId,
        aws_secret_access_key=AWSSecretKey)

    S3Client = boto3.client(
        's3',
        region_name='eu-west-2',
        aws_access_key_id=AWSAccessKeyId,
        aws_secret_access_key=AWSSecretKey)

    app.run(host="0.0.0.0", port=8080)
